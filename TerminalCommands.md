# Flask Deployment script

# TODO: replace: flask-dir, python-venv, flask-app, flask_test_app.py, wsgi.py, flask-app-conf
# find and replace variables for copy-paste use

# TODO: update from deploy-flask.sh

#__parent-dir
#  |_venv-dir    
#  |_app-dir

local parent-dir  = flask-dir  
local venv-dir    = python-venV
local app-dir     = flask-app

local flask-test-app  = flask_test_app.py  
local wsgi            = wsgi.py
local nginx_conf_file = flask-app-conf

#install ubuntu packages - preparation
    
    sudo apt-get update
    sudo apt-get install python3-pip python3-dev 
    
# Prepare Dir and venv # using virtual environment

    sudo mkdir /opt/flask-dir

    # export LC_ALL="en_US.UTF-8"
    # export LC_CTYPE="en_US.UTF-8"

    sudo pip3 install virtualenv

    virtualenv /opt/flask-dir/python-venv

# Prepare Dir and venv # using virtual environment

    sudo mkdir /opt/flask-dir

    # export LC_ALL="en_US.UTF-8"
    # export LC_CTYPE="en_US.UTF-8"

    python3 -m venv /opt/flask-dir/python-venv

# Source venv and install flask and gunicorn 

    #source /opt/flask-dir/python-venv/bin/activate

    #pip3 install flask gunicorn

    sudo /opt/flask-dir/python-venv/bin/pip3 install flask gunicorn

# Copy test application and wsgi entry

    sudo mkdir /opt/flask-dir/flask-app
    
    sudo cp ./flask_test_app.py /opt/flask-dir/flask-app/flask_test_app.py

    # can be skipped if exixts
    sudo cp ./wsgi.py /opt/flask-dir/flask-app/wsgi.py

    # the below command starts the gunicorn 

    # /opt/flask-dir/flask-appsudo /opt/flask-dir/python-venv/bin/gunicorn --bind 0.0.0.0:5000 --pythonpath /opt/flask-dir/flask-app/ wsgi:app

# Copy the  

    sudo cp ./gunicorn.service /etc/systemd/system/gunicorn.service

# nginx installation 
# TODO: replace flask-dir, python-venv, flask-app-conf

    sudo apt-get install nginx

    sudo /opt/flask-dir/python-venv/bin/gunicorn --bind 0.0.0.0:5000 wsgi:app
 
    sudo cp ./flask-app-conf /etc/nginx/sites-available/flask-app-conf
    
    sudo ln -s /etc/nginx/sites-available/flask-app-conf /etc/nginx/sites-enabled


