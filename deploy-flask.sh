#!/bin/bash

#__parent-dir
#  |_venv-dir    
#  |_app-dir
#  |_gunicorn-dir

#  define script variables 

ParentDir="/opt/flask-dir"     # Full path
venv_dir="python-venv"         # directory only       
app_dir="flask-app"            #
gunicorn_dir="gunicorn-dir"


# Name of the copy file 
flask_test_app="flask_test_app.py"  #  should be .py file 
wsgi="wsgi"                         #  wsgi entry point : (skip the .py) the .py will be added to the file but not in the gunicorn file

gunicorn_service="gunicorn.service" # should contain .service in the end
user=flask 

# nginx conf parameters 

nginx_conf_file="nginx-conf"    # 
nginx_domain="flask-domain"
port=80 


# function to ask user for [Y/N]

execute () {
while true; do
    read -p "Proceed with: $1 [Y/N] ?" yn
    case $yn in
        [Yy]* ) $1; break;; # <<<<<
        [Nn]* ) echo "continue"; break;;
        * ) echo "echo "answer Y or N"";;
    esac
done
}

#install ubuntu packages - preparation

    execute 'sudo apt-get update'

    execute 'sudo apt-get install python3-pip python3-dev'

# Prepare Dir and venv # using virtual environment

    execute "sudo mkdir $ParentDir"

    # execute "export LC_ALL=\"en_US.UTF-8\""
    # execute "export LC_CTYPE=\"en_US.UTF-8\""

    # execute "python3 -m venv $ParentDir/$venv_dir"  

    execute "sudo pip3 install virtualenv"

    execute "sudo virtualenv -p python3 $ParentDir/$venv_dir"

# Source venv and install flask and gunicorn 

    #source /opt/flask-dir/python-venv/bin/activate

    #pip3 install flask gunicorn

    execute "sudo $ParentDir/$venv_dir/bin/pip3 install flask gunicorn"

# Copy test application and wsgi entry: skip if exists

    execute "sudo mkdir $ParentDir/$app_dir"
    
    execute "sudo cp ./flask-test-app/flask_test_app.py $ParentDir/$app_dir/$flask_test_app"

    execute "sudo cp ./flask-test-app/wsgi.py $ParentDir/$app_dir/$wsgi.py"

    # the below command starts the gunicorn from cli
    # echo "sudo $ParentDir/$venv_dir/bin/gunicorn --bind 0.0.0.0:5000 --pythonpath $ParentDir/$app_dir/ wsgi:app"

# Create User to use Gunicorn

    execute "sudo mkdir $ParentDir/$gunicorn_dir"

    execute "sudo adduser $user"

    execute "sudo chown $user $ParentDir/$gunicorn_dir"
    execute "sudo chown :$user $ParentDir/$gunicorn_dir"

# Create and/or Copy the gunicorn service file 

gunicorn_service_text=$"[Unit]
Description=Gunicorn instance to serve myproject
After=network.target

[Service]
User=$user
Group=www-data
WorkingDirectory=$ParentDir/$app_dir/
Environment=\"PATH=$ParentDir/$venv_dir/bin/\"
ExecStart=$ParentDir/$venv_dir/bin/gunicorn --workers 3 --bind unix:$ParentDir/$gunicorn_dir/$app_dir.sock -m 007 wsgi:app

[Install]
WantedBy=multi-user.target
"

while true; do
read -p "Proceed with: echo \"$gunicorn_service_text\" > ./gunicorn.service [Y/N] ?" yn
    case $yn in
        [Yy]* ) echo "$gunicorn_service_text" > ./gunicorn.service; break;; # <<<<<
        [Nn]* ) echo "continue"; break;;
        * ) echo "answer Y or N";;
    esac
done

    execute "sudo cp ./gunicorn.service /etc/systemd/system/$gunicorn_service"

    execute "sudo systemctl start $gunicorn_service"

    execute "sudo systemctl enable $gunicorn_service"

# nginx installation

execute "sudo apt-get install nginx"

nginx_conf_text=$"server {
    listen $port;
    server_name $nginx_domain www.$nginx_domain;

    location / {
        include proxy_params;
        proxy_pass http://unix:$ParentDir/$gunicorn_dir/$app_dir.sock;
    }
}"

while true; do
read -p "Proceed with: echo \"$nginx_conf_text\" > ./nginx_conf_file [Y/N] ?" yn
    case $yn in
        [Yy]* ) echo "$nginx_conf_text" > ./nginx_conf_file; break;; # <<<<<
        [Nn]* ) echo "continue"; break;;
        * ) echo "answer Y or N";;
    esac
done

    execute "sudo cp ./nginx_conf_file /etc/nginx/sites-available/$nginx_conf_file"

    execute "sudo ln -s /etc/nginx/sites-available/$nginx_conf_file /etc/nginx/sites-enabled"

# MySQL installation

    execute "sudo apt install mysql-server"

    execute "mysql_secure_installation"

while true; do
read -p "Proceed with mysql commands : mysql -p -u root -e \"CREATE DATABASE flask_db;\"" yn
    case $yn in
        [Yy]* ) mysql -p -u root -e "CREATE DATABASE flask_db;";
                break;; # <<<<<
        [Nn]* ) echo "continue"; break;;
        * ) echo "answer Y or N";;
    esac
done

while true; do
read -p "Proceed with mysql commands : mysql -p -u root -e \"CREATE USER 'flask'@'localhost' IDENTIFIED BY 'password'\" [Y/N] ?" yn
    case $yn in
        [Yy]* ) mysql -p -u root -e "CREATE USER 'flask'@'localhost' IDENTIFIED BY 'password'"
                break;; # <<<<<
        [Nn]* ) echo "continue"; break;;
        * ) echo "answer Y or N";;
    esac
done

while true; do
read -p "Proceed with mysql commands : mysql -p -u root -e \"GRANT ALL PRIVILEGES ON flask_db.* TO 'flask'@'localhost';FLUSH PRIVILEGES;\"" yn
    case $yn in
        [Yy]* ) mysql -p -u root -e "GRANT ALL PRIVILEGES ON flask_db.* TO 'flask'@'localhost';FLUSH PRIVILEGES;";
                break;; # <<<<<
        [Nn]* ) echo "continue"; break;;
        * ) echo "answer Y or N";;
    esac
done

# mysql -p -u root -e "CREATE USER 'flask'@'localhost' IDENTIFIED BY 'password'"
# mysql -p -u root -e "CREATE DATABASE flask_db;"
# mysql -p -u root -e "GRANT ALL PRIVILEGES ON flask_db.* TO 'flask'@'localhost'"
# mysql -p -u root -e "FLUSH PRIVILEGES"
